<?php

namespace App\Core;

class Request
{
    const SINGLE_COMMAND = 1;

    /** @var array|mixed */
    public $options = [];
    /** @var mixed */
    public $command;

    /**
     * Request constructor.
     * @param array $argv
     */
    public function __construct(array $argv)
    {
        /** @var array $requestArray */
        $requestArray = self::match(
            implode('::', $argv)
        );
        $this->command = $requestArray['command'];
        $this->options = $requestArray['options'];
    }

    /**
     * @param string $string
     * @return array
     */
    public static function match(string $string): array
    {
        /** @var string $pattern */
        $pattern = "/([a-z]+_[a-z]+)?::--([a-z]+=[а-яА-ЯЁёa-zA-Z0-9 ]+)/u";
        preg_match_all(
            $pattern,
            $string,
            $matches
        );
        /** @var array $commandArray - remove empty key & values from array */
        $commandArray = array_diff($matches[1], ['']);
        /** @var array $options - explode options to format 'amount=value' */
        $options = array_map(function($item){
            return explode('=', $item);
        }, $matches[2]);

        return [
            'command' => (self::SINGLE_COMMAND === count($commandArray))
                ? implode($commandArray)
                : null,
            'options' => $options
        ];
    }
}
