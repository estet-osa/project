<?php

namespace App\Core;

use App\Dto\OrderDto;
use PDO;

class Db
{
    /** @var self|null */
    private static $_instance;
    /** @var PDO|null */
    protected static $db = null;

    /**
     * @return Db
     */
    public static function getInstance(): self
    {
        self::connect();
        self::createTableIfNotExists();

        if (null === self::$_instance)
            self::$_instance = new self;
        return self::$_instance;
    }

    /**
     * @param OrderDto $orderDto
     * @return $this
     */
    public function insert(OrderDto $orderDto)
    {
        /** @var string $query */
        $query = "INSERT INTO `order`(item,amount,total,discount_card,discount_val) 
          VALUES(
              '$orderDto->item',
              '$orderDto->amount',
              '$orderDto->total',
              '$orderDto->discountCard',
              '$orderDto->discountVal'
          )";
        try {
            self::$db->exec($query);
        } catch (\PDOException $exception) {
            $exception->getMessage();
        }
        return $this;
    }

    /**
     * @return array
     */
    public function select(): array
    {
        $query = self::$db->prepare("SELECT * FROM `order`");
        $query->execute();

        return $query->fetchAll(PDO::FETCH_ASSOC);
    }

    /**
     * Project.php forming to project and use as db name.
     *
     * @return string
     */
    public static function getDbPathAndName(): string
    {
        /** @var string $dbName */
        $dbName =
            'sqlite:' . __DIR__ . '/../Data/'
            . str_replace('.php', '', $_SERVER['PHP_SELF'])
            . '.sqlite';
        return $dbName;
    }

    /**
     * Connect to database.
     *
     * @return void
     */
    protected static function connect(): void
    {
        if (!is_dir($dir = __DIR__ . '/../Data/')) {
            mkdir($dir);
        }
        try {
            /** @var PDO db */
            self::$db = new PDO(self::getDbPathAndName());
        } catch (\PDOException $exception) {
            $exception->getMessage();
        }
    }

    /**
     * Try create table if not exists on database.
     *
     * @return void
     */
    protected static function createTableIfNotExists(): void
    {
        /** @var string $query */
        $query = "CREATE TABLE IF NOT EXISTS `order` (
            `id` INTEGER PRIMARY KEY AUTOINCREMENT, 
            `item` TEXT NOT NULL,
            `amount`NUMERIC,
            `total` FLOAT NOT NULL,
            `discount_card` NUMERIC,
            `discount_val` NUMERIC)"
        ;
        try {
            self::$db->exec($query);
        } catch (\PDOException $exception) {
            $exception->getMessage();
        }
    }

    private function __construct(){}
    private function __clone(){}
    private function __wakeup(){}
}
