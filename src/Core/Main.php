<?php

namespace App\Core;

use App\Core\Order;

class Main
{
    /** @var Request|null */
    public $request;

    function __construct(Request $request)
    {
        $this->request = $request;
    }

    /**
     * @return Main
     */
    function make(): self
    {
        (new Order)
            ->execute($this->request);
        return $this;
    }
}
