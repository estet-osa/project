<?php

namespace App\Core;

use App\Dto\OrderDto;

class Order
{
    /**
     * @param Request $request
     * @return $this
     */
    public function execute(Request $request)
    {
        if ($request->command == 'create_order')
            $this->create($request->options);

        if ($request->command == 'list_order')
            $this->list();

        return $this;
    }

    /**
     * @param array $options
     */
    public function create(array $options): void
    {
        Db::getInstance()->insert(
            OrderDto::fromArray($options)
        );
    }

    public function list()
    {
        $result = Db::getInstance()->select();

        print_r($result);
    }
}
