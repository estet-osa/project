<?php

namespace App\Dto;

/**
 * Class OrderDto
 * @package App\Dto
 */
class OrderDto
{
    public $item, $amount, $total, $discountCard, $discountVal, $num;

    /**
     * @param array $options
     * @return $this
     */
    public function load(array $options)
    {
        foreach ($options as $option) {
            if (in_array('item', $option)) {
                $this->item = $option[1];
            }
            if (in_array('amount', $option)) {
                $this->amount = $option[1];
            }
            if (in_array('total', $option)) {
                $this->total = $option[1];
            }
            if (in_array('discount_card', $option)) {
                $this->discountCard = $option[1];
            }
            if (in_array('discount_val', $option)) {
                $this->discountVal = $option[1];
            }
            if (in_array('num', $option)) {
                $this->num = $option[1];
            }
        }
        return $this;
    }

    /**
     * @param array $options
     * @return $this
     */
    public static function fromArray(array $options)
    {
        return (new self())->load($options);
    }
}
