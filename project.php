<?php

require_once realpath('vendor/autoload.php');

use App\Core\{Main, Request};

(new Main(new Request($argv)))
    ->make();
